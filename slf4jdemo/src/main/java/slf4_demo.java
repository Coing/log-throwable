
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Coing on 2017/5/16.
 */
public class slf4_demo {
    /* Get actual class name to be printed on */
    static Logger log = LoggerFactory.getLogger(
            slf4_demo.class.getName());

    public static void main(String[] args)
            throws IOException,SQLException{

        log.debug("Hello this is an debug message");
        log.info("Hello this is an info message");
    }
}

