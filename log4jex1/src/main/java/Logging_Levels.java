import org.apache.log4j.Level;
import org.apache.log4j.Logger;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

/**
 * Created by Coing on 2017/5/16.
 */
public class Logging_Levels {

    //static final Logger log = LogManager.getLogger(Logging_Levels.class.getName());
        private static org.apache.log4j.Logger log = Logger
            .getLogger(LogClass.class);
    public static void main(String[] args) {
        //log.setLevel(Level.WARN);

        log.trace("Trace Message!");
        log.debug("Debug Message!");
        log.info("Info Message!");
        log.warn("Warn Message!");
        log.error("Error Message!");
        log.fatal("Fatal Message!");
    }
}
