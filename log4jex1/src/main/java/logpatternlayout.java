import org.apache.log4j.Logger;
/*import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;*/

import java.io.*;
import java.sql.SQLException;

/**
 * Created by Coing on 2017/5/16.
 */
public class logpatternlayout {
    /* Get actual class name to be printed on */
    static Logger log = Logger.getLogger(
            logpatternlayout.class.getName());
    //static final Logger log = LogManager.getLogger(logpatternlayout.class.getName());

    public static void main(String[] args)
            throws IOException,SQLException{

        log.debug("Hello this is an debug message");
        log.info("Hello this is an info message");
    }
}

