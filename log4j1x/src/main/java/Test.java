/**
 * Created by Coing on 2017/6/18.
 */

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

public class Test {
    private static Logger logger = Logger.getLogger(Test.class);

    public static void main(String[] args) throws InterruptedException {

        PropertyConfigurator.configure(ClassLoader.getSystemResource("log4j4.properties"));

        for (int i = 0; i < 200; i++) {
            //Thread.sleep(1000);
            logger.trace("This is trace message.");
            // 记录debug级别的信息
            logger.debug("This is debug message.");
            // 记录info级别的信息
            logger.info("This is info message.");
            // 记录error级别的信息
            logger.error("This is error message.");
        }

    }
}
