

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Coing on 2017/5/15.
 */
public class log4jExample{
    /* Get actual class name to be printed on */

/*    static Logger log = Logger.getLogger(
            log4jExample.class.getName());*/
    static final Logger log = LogManager.getLogger(log4jExample.class.getName());

    public static void main(String[] args) {

        log.debug("Hello this is an debug message");
        log.info("Hello this is an info message");
    }
}